var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    usernames= [];

server.listen(process.env.PORT || 3000);

console.log('server running....');

app.get('/',function(req,res){
     res.sendFile(__dirname + '/index.html');
});

io.sockets.on('connection',function(socket){
    console.log('Socket connected.');

    //add new user event
    socket.on('new user',function(data,callback){
        if(usernames.indexOf(data)!=-1){
            callback(false);
        }else{
            callback(true);
            socket.username = data;
            usernames.push(socket.username);
            //console.log(socket.username+" is added");
            updateUsernames();
        }
    });

    //update username in user list 
    function updateUsernames(){
        io.sockets.emit('usernames',usernames);
    }

    //System message of user joining  
    socket.on('join user',function(from,to){
        //onsole.log('Move to client for system message '+from+' '+to);
        io.sockets.emit('system message',{user: from,msg: to});
    });

    //Send Message event
    socket.on('send message',function(data){
        //console.log('Message :'+data);
        io.sockets.emit('new message',{msg: data,user:socket.username});
    });
    
    //Disconnecting user
    socket.on('disconnect user',function(data){
    	socket.username=data;
    	console.log(usernames[0]);
        usernames.splice(usernames.indexOf(socket.username),1);
        updateUsernames();
        socket.disconnect();
    });
   
});
